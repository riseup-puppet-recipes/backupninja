require 'spec_helper'

describe 'backupninja::action' do
  context 'supported operating systems' do
    on_supported_os.each do |os, facts|
      context "on #{os}" do
        let(:facts) { facts }

        let(:title ) { 'backupjob' }

        let(:params) { {
            :type     => 'mysql',
            :content  => 'foo = bar',
            :validate => false,
        } }

        it { is_expected.to compile.with_all_deps }

        context 'with defaults' do
          it {
            is_expected.to contain_file('/etc/backup.d/50_backupjob.mysql').with(
              :content => /foo = bar/,
              :owner   => 'root',
              :group   => 'root',
              :mode    => '0600',
            )
          }
        end

        context 'with ensure set to disabled' do
          let(:params) do
            super().merge({ :ensure => 'disabled' })
          end

          it { is_expected.to contain_file('/etc/backup.d/50_backupjob.mysql.disabled') }
        end

        context 'with custom order set' do
          let(:params) do
            super().merge({ :order => 10 })
          end

          it { is_expected.to contain_file('/etc/backup.d/10_backupjob.mysql') }
        end

        context 'with both content and source set' do
          let(:params) do
              super().merge({ :source => 'puppet:///modules/profile/backupninja/backupjob.mysql' })
          end

          it { is_expected.to compile.and_raise_error(/backupninja::action cannot have both content and source/) }
        end

        context 'with neither content or source set' do
          let(:params) do
              super().merge({ :content => :undef })
          end

          it { is_expected.to compile.and_raise_error(/backupninja::action must have either content or source/) }
        end

        context 'with validate set to true' do
          let(:params) do
              super().merge({ :validate => true })
          end

          it { is_expected.to contain_file('/etc/backup.d/50_backupjob.mysql').without_validate_cmd( :undef ) }
        end

      end
    end
  end
end
