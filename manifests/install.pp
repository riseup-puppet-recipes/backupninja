# This class handles installing the backupninja package.
#
# @private
#
class backupninja::install {

  assert_private()

  if $backupninja::manage_packages {
    package { 'backupninja':
      ensure => $backupninja::ensure_backupninja_version
    }
  }

}

