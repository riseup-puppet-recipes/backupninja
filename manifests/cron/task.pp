# This defined task manages a backupninja cron task.
#
# @param file
#   Specifies the full file path of the cron job.
#
# @param ensure
#   Whether the cron task should be present or absent.
#
# @param test_command
#   A command to run, and which must return true, for the cron task to run.
#   The default command is to test whether the backupninja executable exists.
#
# @param command
#   The command to run as part of the cron task. The default is to run
#   backupninja.
#
# @param command_options
#   Command-line options to append to the command above. The default is to run
#   the command without arguments.
#
# @param min
#   Specifies the minute component of the cron job.
#
# @param hour
#   Specifies the hour component of the cron job.
#
# @param dom
#   Specifies the day of month component of the cron job.
#   Default value: '*'
#
# @param month
#   Specifies the month component of the cron job.
#   Default value: '*'
#
# @param dow
#   Specifies the day of week component of the cron job.
#   Default value: '*'
#
# @example
#   backupninja::cron::task { 'backuptime':
#     file            => '/etc/cron.d/backuptime',
#     command_options => '--now --run /etc/backup.d/mybackup.sh',
#     min             => '45',
#     hour            => '4',
#     dom             => '30',
#   }
#
define backupninja::cron::task (
  Stdlib::Absolutepath $file,
  String $min,
  String $hour,
  Enum['present', 'absent'] $ensure = 'present',
  Optional[String] $test_command = $backupninja::cron_test_command,
  Optional[String] $command = $backupninja::cron_command,
  Optional[String] $command_options = $backupninja::cron_command_options,
  String $dom = '*',
  String $month = '*',
  String $dow = '*',
) {

  $_test_command = $test_command ? {
    undef   => "[ -x ${backupninja::executable} ]",
    default => $test_command
  }

  $_command = $command ? {
    undef   => $backupninja::executable,
    default => $command
  }

  $_run_command = $command_options ? {
    undef   => $_command,
    default => "${_command} ${command_options}"
  }

  $cron = {
    'min'          => $min,
    'hour'         => $hour,
    'dom'          => $dom,
    'month'        => $month,
    'dow'          => $dow,
    'test_command' => $_test_command,
    'run_command'  => $_run_command,
  }

  file { $file:
    ensure  => $ensure,
    content => epp('backupninja/cron.epp', $cron),
    owner   => 'root',
    group   => 'root',
    mode    => '0644'
  }

}
