# Safe MySQL dumps, as part of a backupninja run.
#
define backupninja::action::mysql (
  String $ensure                   = 'present',
  Integer $order                   = 10,
  Optional[String] $when           = undef,
  Optional[Boolean] $validate      = undef,
  # authentication
  Optional[String] $user           = undef,
  Optional[String] $dbusername     = undef,
  Optional[String] $dbpassword     = undef,
  Optional[String] $dbhost         = 'localhost',
  Optional[String] $configfile     = $backupninja::mysql_configfile,
  # backup config
  String $backupdir                = '/var/backups/mysql',
  String $databases                = 'all',
  Optional[String] $nodata         = undef,
  Boolean $sqldump                 = false,
  Boolean $hotcopy                 = false,
  Boolean $compress                = true,
  Optional[String] $sqldumpoptions = '--lock-tables --complete-insert --add-drop-table --quick --quote-names',
) {

  if empty($databases) {
    fail('Must define one or more databases to backup!')
  }
  elsif (!$sqldump and !$hotcopy) {
    fail('At least one of sqldump or hotcopy must be enabled!')
  }

  $config = {
    'when'           => $when,
    'user'           => $user,
    'dbusername'     => $dbusername,
    'dbpassword'     => $dbpassword,
    'dbhost'         => $dbhost,
    'configfile'     => $configfile,
    'backupdir'      => $backupdir,
    'databases'      => $databases,
    'nodata'         => $nodata,
    'sqldump'        => $sqldump,
    'hotcopy'        => $hotcopy,
    'compress'       => $compress,
    'sqldumpoptions' => $sqldumpoptions,
  }

  backupninja::action { $name:
    ensure   => $ensure,
    order    => $order,
    type     => 'mysql',
    content  => epp('backupninja/action.epp', { 'config' => $config } ),
    validate => $validate,
  }

}
