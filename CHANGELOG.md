# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## 4.0.4

Word Backup Day 2018 release!

### Fixed

- Missing [source] parameters for rdiff actions

## 4.0.3

### Fixed

- Compatibility with Hiera 5
- Undefined variable warning in backupninja::server
- Default value for backupninja::selections_package_name

### Added

- Unit tests based on rspec-puppet
- Private-class assertions

## 4.0.2

- Improve support for non-Debian based systems

## 4.0.1

### Added

In "metadata.json":
- Missing `operatingsystem_support` section
- Better description string

## 4.0.0

This is the first official release.

Users of the pre-release (legacy) version should consult UPGRADING.md for upgrade information.
